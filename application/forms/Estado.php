<?php

class Application_Form_Estado extends Twitter_Bootstrap_Form_Inline {

    public function init() {
        $this->setMethod('post');

        $nome =  new Zend_Form_Element_Text('nome', array(
        	'label' => 'Nome',
            'placeholder' => 'Digite aqui o nome do estado',
        	'class'   => 'focused',
        	'required' => true
        ));
        
        $nome->addFilter(new Zend_Filter_StringToUpper());
        $this->addElement($nome);
        
        $uf =  new Zend_Form_Element_Text('uf', array(
        		'label' => 'UF',
        		'placeholder' => 'Digite aqui o UF do estado',
        		'class'   => '',
        		'required' => true
        ));
        
        $uf->addFilter(new Zend_Filter_StringToUpper());
        $this->addElement($uf);
        
        $submit = new Zend_Form_Element_Submit('enviar', array(
        	'label' => 'Cadastrar',
        	"class" => "btn btn-large btn-block btn-success"
        ));
        $this->addElement($submit);

       
    }

}
