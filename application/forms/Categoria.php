<?php

class Application_Form_Categoria extends Twitter_Bootstrap_Form_Inline {

    public function init() {
        $this->setMethod('post');

        $categoria =  new Zend_Form_Element_Text('categoria', array(
        	'label' => 'Nome da categoria',
            'placeholder' => 'Digite aqui o nome da categoria',
        	'class'   => 'focused',
        	'required' => true
        ));
        
        $categoria->addFilter(new Zend_Filter_StringToUpper());
        $this->addElement($categoria);
        
        $submit = new Zend_Form_Element_Submit('enviar', array(
        	'label' => 'Cadastrar',
        	"class" => "btn btn-large btn-block btn-success"
        ));
        $this->addElement($submit);

       
    }

}
