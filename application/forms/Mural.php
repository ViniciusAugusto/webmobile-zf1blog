<?php

class Application_Form_Mural extends Twitter_Bootstrap_Form_Inline {
	
	public function init(){
		//nome, email, recado, captcha, termos, submit
		
		$nome = new Zend_Form_Element_Text("nome",array(
			"placeholder" => "Nome Completo",
			"required" => true,
			'class'   => 'focused'
		));
		
		$nome->addValidator(new Zend_Validate_StringLength(array(
			"min" => 10
		)));
		
		$nome->addFilter(new Zend_Filter_StringToUpper());
		
		$this->addElement($nome);
		
		$email = new Zend_Form_Element_Text("email",array(
			"placeholder" => "E-mail",
			"required" => true
		));
		
		$email->addValidator(new Zend_Validate_EmailAddress());
		
		$this->addElement($email);
		
		$recado = new Zend_Form_Element_Textarea("recado",array(
			"placeholder" => "Recado",
			"rows" => 6,
			"required" => true
		));
		
		$this->addElement($recado);
		
		$termos = new Zend_Form_Element_Checkbox("termos",array(
				"label" => "Aceito os termos de uso",
				"required" => true
		));
		
		$termos->addValidator(new Zend_Validate_NotEmpty());
		$termos->addFilter(new Zend_Filter_Null());
		
		$this->addElement($termos);
		
		$this->addElement('captcha', 'captcha', array(
				'placeholder'      => 'Digite os codigo ao lado',
				'required'   => true,
				'captcha'    => array(
						'captcha' => 'Image',
						'wordLen' => 5,
						'timeout' => 300,
						'font' => '/var/www/html/WebMobie/php/frameworks-php/pos-web-2016/zf1-blog/public/fonts/arial.ttf',
						'imgurl'=>'http://localhost/WebMobie/php/frameworks-php/pos-web-2016/zf1-blog/public/images/captcha/',
				)
		));
		
		
		
		
		
		$submit = new Zend_Form_Element_Submit("Enviar",array(
			"class" => "btn btn-large btn-block btn-success"
		));
		
		$this->addElement($submit);
		
		
		
	}
}