<?php

class Application_Form_Cidade extends Twitter_Bootstrap_Form_Inline {

    public function init() {
        $this->setMethod('post');
        

        $estado = new Zend_Form_Element_Select('idestado',array(
        	'label' => 'Estado',
        	'required' => true,
        	'multiOptions' => $this->estados()
        ));
        
        $this->addElement($estado);
       	$estado->addFilter(new Zend_Filter_Null()); 
        
        $descricao = new Zend_Form_Element('descricao', array(
        	'label' => 'Nome da Cidade',
        	'required' => true
        ));
        
        $this->addElement($descricao);
        $descricao->addFilter(new Zend_Filter_Null());
     
        
        $submit = new Zend_Form_Element_Submit("Salvar",array(
			"class" => "btn btn-large btn-block btn-success"
		));
		
		$this->addElement($submit);
    }
    
    private function estados(){
    	
    	$tab = new Application_Model_DbTable_Estado();
    	$estados = $tab->fetchAll(NULL, 'nome');
    	
    	$retorno['0'] = 'Selecione uma opcao';
    	
    	foreach ($estados as $estado):
    		$retorno[$estado->idestados] = $estado->nome;
    	endforeach;
    	
    	return $retorno;
    	
    	
    }

}
