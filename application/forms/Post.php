<?php

class Application_Form_Post extends Twitter_Bootstrap_Form_Inline {

    public function init() {
        $this->setMethod('post');
        

        $categoria = new Zend_Form_Element_Select('idcategoria',array(
        	'label' => 'Categoria',
        	'required' => true,
        	'multiOptions' => $this->categorias()
        ));
        
        $this->addElement($categoria);
       	$categoria->addFilter(new Zend_Filter_Null()); 
        
        $titulo = new Zend_Form_Element('titulo', array(
        	'label' => 'Titulo do Post',
        	'required' => true
        ));
        
        $min10 = new Zend_Validate_StringLength(array(
        	'min' => 10
        ));
        
        $titulo->addValidator($min10);
        
        $titulo->addFilter(new Zend_Filter_StringToUpper());
        
        $this->addElement($titulo);
        
        $texto = new Zend_Form_Element_Textarea('texto', array(
        	'label' => 'Conteudo',
        	'required' => true,
        	"placeholder" => "Digite o texto do post",
        	"rows" => 6,
        ));
        
        $this->addElement($texto);
        
        $submit = new Zend_Form_Element_Submit("Salvar",array(
			"class" => "btn btn-large btn-block btn-success"
		));
		
		$this->addElement($submit);
    }
    
    private function categorias(){
    	
    	$tab = new Application_Model_DbTable_Categoria();
    	$categorias = $tab->fetchAll(NULL, 'categoria');
    	
    	$retorno['0'] = 'Selecione uma opcao';
    	
    	foreach ($categorias as $categoria):
    		$retorno[$categoria->idcategoria] = $categoria->categoria;
    	endforeach;
    	
    	return $retorno;
    	
    	
    }

}
