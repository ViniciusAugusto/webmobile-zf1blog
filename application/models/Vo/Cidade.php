<?php

class Application_Model_Vo_Cidade extends Application_Model_DbTable_Cidade {
	private $id, $descricao, $idestado; 

	function getIdEstado() {
		return $this->idestado;
	}

	function getDescricao() {
		return $this->descricao;
	}
	
	function getId() {
		return $this->id;
	}

	function setIdestado($idestado) {
		$this->idestado = $idestado;
	}

	function setDescricao($descricao) {
		$this->descricao = $descricao;
	}
	
	function setId($id){
		$this->id = $id;
	}

}