<?php

class Application_Model_Vo_Estado extends Application_Model_DbTable_Estado {
	private $idestados, $nome, $uf; 

	function getIdEstado() {
		return $this->idestados;
	}

	function getNome() {
		return $this->nome;
	}
	
	function getUf() {
		return $this->uf;
	}

	function setIdestado($idestado) {
		$this->idestados = $idestado;
	}

	function setNome($nome) {
		$this->nome = $nome;
	}
	
	function setUf($uf){
		$this->uf = $uf;
	}

}