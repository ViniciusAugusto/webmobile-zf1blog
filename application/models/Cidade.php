<?php

class Application_Model_Cidade {

    public function apagar($id) {
    	$tab = new Application_Model_DbTable_Cidade();
    	$tab->delete('id = '.$id);
    	
    	return true;
    }

    public function atualizar(Application_Model_Vo_Cidade $cidade) {
    	$tab = new Application_Model_DbTable_Cidade();
    	$tab->update(array(
    			'id' => $cidade->getId(),
    			'descricao' => $cidade->getDescricao(),
    			'idestado' => $cidade->getIdEstado()
    	), 'id = '.$cidade->getId());
    	return true;
    }

    public function salvar(Application_Model_Vo_Cidade $cidade) {
        $tab = new Application_Model_DbTable_Cidade();
        $tab->insert(array(
        	'idestado' => $cidade->getIdEstado(),
        	'descricao' => $cidade->getDescricao()
        ));
        return true;
    }

}
