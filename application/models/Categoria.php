<?php

class Application_Model_Categoria {

    public function apagar($idcategoria) {
    	
    	$postModelDbTable = new Application_Model_DbTable_Post();
    	$post = $postModelDbTable->fetchRow('idcategoria = '.$idcategoria);
    	
    	if($post !== NULL){
    		throw new Exception('Existe post cadastrados nesta categoria',1);
    	}
    	
        $tab = new Application_Model_DbTable_Categoria();
        $tab->delete('idcategoria = '.$idcategoria);
        
        return true;
    }

    public function atualizar(Application_Model_Vo_Categoria $categoria) {
        $tab = new Application_Model_DbTable_Categoria();
        $tab->update(array(
        	'categoria' => $categoria->getCategoria()
        ), 'idcategoria = '.$categoria->getIdcategoria());
        
        return true;
    }

    public function salvar(Application_Model_Vo_Categoria $categoria) {
        $tab = new Application_Model_DbTable_Categoria();
        $tab->insert(array(
        	'categoria' => $categoria->getCategoria()
        ));
        
        $categoria->setIdcategoria($tab->getAdapter()->lastInsertId());
        
        
        return true;
    }

}
