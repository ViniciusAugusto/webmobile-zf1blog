<?php

class Application_Model_Estado {

    public function apagar($idestado) {
    	
        $tab = new Application_Model_DbTable_Estado();
        $tab->delete('idestados = '.$idestado);
        
        return true;
    }

    public function atualizar(Application_Model_Vo_Estado $estado) {
        $tab = new Application_Model_DbTable_Estado();
        $tab->update(array(
        	'nome' => $estado->getNome(),
        	'uf' => $estado->getUf(),
        ), 'idestados = '.$estado->getIdEstado());
        
        return true;
    }

    public function salvar(Application_Model_Vo_Estado $estado) {
        $tab = new Application_Model_DbTable_Estado();
        $tab->insert(array(
        	'nome' => $estado->getNome(),
        	'uf' => $estado->getUf(),
        ));
        
        $estado->setIdestado($tab->getAdapter()->lastInsertId());
       
        return true;
    }

}
