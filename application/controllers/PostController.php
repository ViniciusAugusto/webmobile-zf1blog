<?php

class PostController extends Blog_Controller_Action {

    public function indexAction() {
    	
		$adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$select = $adapter->select("")
		  				  ->from(array("p" => "post"),array('idpost','titulo'))
		  				  ->joinInner(array("c" => "categoria"), 'c.idcategoria = p.idcategoria', array('categoria'))
						  ->order('idpost DESC');

    	
    	$posts = $select->query()->fetchAll();
    	
    	$this->view->posts = $posts;
    	$this->view->podeApagar = $this->aclIsAllowed('post', 'delete');
    }

    public function createAction() {
    	
        $form = new Application_Form_Post();
        
        $this->view->form = $form;
         
        if($this->getRequest()->isPost()){
        	$post = $this->getAllParams();
        
        	if($form->isValid($post)){
        		$posts = $form->getValues();
        		
        		$auth = Zend_Auth::getInstance();
        		$dados = $auth->getIdentity();
        		 
        		$vo = new Application_Model_Vo_Post();
        		$vo->setIdcategoria($post['idcategoria']);
        		$vo->setIdadmin($dados->idadmin);
        		$vo->setTexto($post['texto']);
        		$vo->setTitulo($post['titulo']);
        		 
        		$model = new Application_Model_Post();
        		$model->salvar($vo);
        		 
        		$flash = $this->_helper->flashMessenger;
        		 
        		$flash->addMessage("Registro salvo");
        		 
        		$this->_helper->Redirector->gotoSimpleAndExit('index');
        		 
        	}
        }
    }

    public function deleteAction() {
    	
    	$idpost = (int) $this->getParam("idpost");
    	 
    	$model = new Application_Model_Post();
    	 
    	$flash = $this->_helper->flashMessenger;
    	 
    	try{
    		$model->apagar($idpost);
    		$flash->addMessage("Registro apagado");
    	}catch (Exception $e){
    		$flash->addMessage($e->getMessage());
    	}
    	
    	$this->_helper->Redirector->gotoSimpleAndExit('index');
        
    }

    public function updateAction() {
        
    	$idpost = (int) $this->getParam("idpost");
    	
    	$tab = new Application_Model_DbTable_Post();
    	$posts = $tab->fetchRow("idpost = $idpost");

		if($posts === NULL){
			
			$flash = $this->_helper->flashMessenger;
			
			$flash->addMessage("Registro nao existe");
			 
			$this->_helper->Redirector->gotoSimpleAndExit('index');
		}
    	
    	$form = new Application_Form_Post();
    	 
    	$this->view->form = $form;
    	 
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    	
    		if($form->isValid($post)){
    			
    			$post = $form->getValues();
    			 
    			$vo = new Application_Model_Vo_Post();
    			$vo->setIdpost($idpost);
        		$vo->setIdcategoria($post['idcategoria']);
        		$vo->setIdadmin('1');
        		$vo->setTexto($post['texto']);
        		$vo->setTitulo($post['titulo']);
        		 
        		$model = new Application_Model_Post();
        		$model->atualizar($vo);
    			 
    			$flash = $this->_helper->flashMessenger;
    			 
    			$flash->addMessage("Registro atualizado");
    			 
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			 
    		}
    	}else{
    		$form->populate($posts->toArray());
    	}
    	
    }

}
