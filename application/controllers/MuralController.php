<?php

class MuralController extends Blog_Controller_Action{

	public function indexAction(){

		$form = new Application_Form_Mural();
		
		if($this->getRequest()->isPost()){
			
			$post = $this->getAllParams();
			
			if($form->isValid($post)){
				echo "<pre>";
				print_r($form->getValues());
				exit;
			}
			
		}
		
		$this->view->form = $form;
	}

}