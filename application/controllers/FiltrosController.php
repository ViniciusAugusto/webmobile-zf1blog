<?php
class FiltrosController extends Blog_Controller_Action {
	public function digitosAction() {
		$filter = new Zend_Filter_Digits ();
		echo $filter->filter ( 'Maio/2016' );
		exit ();
	}
	public function lowerAction() {
		$f = new Zend_Filter_StringToLower ();
		
		echo $f->filter ( "PR" );
		
		exit ();
	}
	public function upperAction() {
		$a = new Zend_Filter_StringToUpper ();
		
		echo $a->filter ( "vinicius" );
		
		exit ();
	}
	public function htmlAction() {
		$valor = "<h1><marquee><strong>FORA DILMA</strong><marquee></h1>";
		
		$f = new Zend_Filter_HtmlEntities ();
		
		echo $f->filter ( $valor );
		
		exit ();
	}
	public function zipAction() {

		$dir = "/var/www/html/WebMobie/php/frameworks-php/pos-web-2016/zf1-blog/public";
		
		$f = new Zend_Filter_Compress ( array (
				"adapter" => "Zip",
				"options" => array (
						"archive" => $dir . "/arquivo.zip" 
				) 
		) );
		
		echo $f->filter($dir . "/index.php");
		
		
		exit;
	}
	
	public function filtroAction(){
		
		$f = new Zend_Filter();
		
		$f->addFilter(new Zend_Filter_Alpha(array(
			"allowwhitespace" => true
		)));
		$f->addFilter(new Zend_Filter_StringToLower());
		$f->addFilter(new Zend_Filter_StringTrim());
		
		echo $f->filter("    FoRa DilMA #tchauQuerida!  2016   ");
		
		exit;
		
	}
}