<?php

class AulaController extends Blog_Controller_Action {

    public function aulaAction() {
        $params = $this->getAllParams();

        $id = $this->getParam("id",0);
		$nome = $this->getParam("nome");
		
		
        $this->view->id = $id;
        
        $this->view->nome = $nome;
        
        if($this->hasParam("idade"))
       		$this->view->idade = $this->getParam("idade");

    }
    
    public function somarAction(){
    	
    	$valor1 = $this->getParam("valor1");

    	$valor2 = $this->getParam("valor2");
    	
    	$resultado = $valor1 + $valor2;
    	
    	$this->view->resultado = $resultado;
    }

}