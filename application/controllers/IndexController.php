<?php

class IndexController extends Blog_Controller_Action {

    public function indexAction() {
    	
    	
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $select = $adapter->select()
        				  ->from(array("p" => "post"),array('idpost','titulo'))
        				  ->joinInner(array("c" => "categoria"), 'c.idcategoria = p.idcategoria',array('categoria','idcategoria'))
        				  ->order('idpost DESC');
        
        $idcategoria = (int) $this->getParam("idcategoria",0);
        
        if($idcategoria > 0){
        	$select->where("p.idcategoria = ".$idcategoria);
        }
        
        $posts = $select->query()->fetchAll();
        
        $this->view->posts = $posts;
       
    }

    public function categoriasAction() {
    	
        $categoriasModelDbTable = new Application_Model_DbTable_Categoria();
        
        $categorias = $categoriasModelDbTable->fetchAll(NULL, "idcategoria DESC");
        
        $this->view->categorias = $categorias;
    }

    public function postAction() {
    	
        $idpost = (int) $this->getParam("idpost",0);
        
        if($idpost > 0){
        	
        	$adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        	
        	$select = $adapter->select()
        	->from(array("p" => "post"),array('idpost','titulo', 'texto'))
        	->joinInner(array("c" => "categoria"), 'c.idcategoria = p.idcategoria',array('categoria'))
        	->where('idpost = '.$idpost)
        	->limit(1);
        	
        	$post = $select->query()->fetch();
        	
        	$this->view->post = $post;
        	
        }else{
        	
        	throw new Zend_Controller_Action_Exception('Post nao encontrado');
        }
        
        
        
    }

}
