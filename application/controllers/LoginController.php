<?php

class LoginController extends Blog_Controller_Action {

    public function indexAction() {
        $form = new Application_Form_Login();
        
        if ($this->getRequest()->isPost()) {
            $values = $this->getAllParams();
            if ($form->isValid($values)) {
                // autenticacao do usuario
                
            	$dbTable = new Zend_Auth_Adapter_DbTable();
            	$dbTable->setTableName("admin");
            	$dbTable->setCredentialColumn("senha");
            	$dbTable->setIdentityColumn("email");
            	
            	$dbTable->setIdentity($form->getValue("email"));
            	$dbTable->setCredential($form->getValue("senha"));
            	
            	$auth = Zend_Auth::getInstance();
            	$autenticacao = $auth->authenticate($dbTable);
            	
            	$flash = $this->_helper->flashMessenger;
            	
            	if($autenticacao->isValid()){
            		
            		$dados = $dbTable->getResultRowObject(NULL, array("senha"));
            		
            		/*echo "<pre>";
            		print_r($dados);
            		exit;*/
            		
            		$auth->getStorage()->write($dados);
            		$this->_helper->Redirector->gotoSimpleAndExit('index','index');
            		
            	}else{
            		$flash->addMessage("Falha ao autenticar");
            		$this->_helper->Redirector->gotoSimpleAndExit('index');
            	}
            	
            }
        }
        
        $this->view->form = $form;
    }

    public function logoutAction() {
        
    	Zend_Auth::getInstance()->clearIdentity();
    	
    	$this->_helper->Redirector->gotoSimpleAndExit('index');
    	
    	
    	
    	
    }

}
