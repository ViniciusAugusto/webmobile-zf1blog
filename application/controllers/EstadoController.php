<?php

class EstadoController extends Blog_Controller_Action {

    public function indexAction() {
    	
        $tab = new Application_Model_Vo_Estado();
        
        $estados = $tab->fetchAll(NULL, 'nome ASC')->toArray();
        
        $this->view->estados = $estados;
    }

    public function createAction() {
        
    	$form = new Application_Form_Estado();
    	
    	$this->view->form = $form;
    	
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    		
    		if($form->isValid($post)){
    			$posts = $form->getValues();
    			
    			$vo = new Application_Model_Vo_Estado();
    			$vo->setNome($post['nome']);
    			$vo->setUf($post["uf"]);
    			
    			$model = new Application_Model_Estado();
    			$model->salvar($vo);
    			
    			$flash = $this->_helper->flashMessenger;
    			
    			$flash->addMessage("Registro salvo");
    			
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			
    		}
    	}
    	
    }

    public function deleteAction() {
        
    	$idestado = (int) $this->getParam("idestado");
    	
    	$model = new Application_Model_Estado();
    	
    	$flash = $this->_helper->flashMessenger;
    	
		try{
			$model->apagar($idestado);
			$flash->addMessage("Registro apagado");
		}catch (Exception $e){
			$flash->addMessage($e->getMessage());
		}
		
		$this->_helper->Redirector->gotoSimpleAndExit('index');
    	
    }

    public function updateAction() {
    	
    	$idestado = (int) $this->getParam("idestado");
    	
    	$tab = new Application_Model_DbTable_Estado();
    	$estado = $tab->fetchRow("idestados = $idestado");

		if($estado === NULL){
			
			$flash = $this->_helper->flashMessenger;
			
			$flash->addMessage("Registro nao existe");
			 
			$this->_helper->Redirector->gotoSimpleAndExit('index');
		}
    	
    	$form = new Application_Form_Estado();
    	 
    	$this->view->form = $form;
    	 
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    	
    		if($form->isValid($post)){
    			$posts = $form->getValues();
    			 
    			$vo = new Application_Model_Vo_Estado();
    			$vo->setNome($post['nome']);
    			$vo->setUf($post["uf"]);
    			$vo->setIdestado($idestado);
    			 
    			$model = new Application_Model_Estado();
    			$model->atualizar($vo);
    			 
    			$flash = $this->_helper->flashMessenger;
    			 
    			$flash->addMessage("Registro atualizado");
    			 
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			 
    		}
    	}else{
    		$form->populate($estado->toArray());
    	}
    }

}
