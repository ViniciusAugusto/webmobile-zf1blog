<?php

class CategoriaController extends Blog_Controller_Action {

    public function indexAction() {
    	
        $tab = new Application_Model_Vo_Categoria();
        
        $categorias = $tab->fetchAll(NULL, 'idcategoria DESC')->toArray();
        
        $this->view->categorias = $categorias;
    }

    public function createAction() {
        
    	$form = new Application_Form_Categoria();
    	
    	$this->view->form = $form;
    	
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    		
    		if($form->isValid($post)){
    			$posts = $form->getValues();
    			
    			$vo = new Application_Model_Vo_Categoria();
    			$vo->setCategoria($post['categoria']);
    			
    			$model = new Application_Model_Categoria();
    			$model->salvar($vo);
    			
    			$flash = $this->_helper->flashMessenger;
    			
    			$flash->addMessage("Registro salvo");
    			
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			
    		}
    	}
    	
    }

    public function deleteAction() {
        
    	$idcategoria = (int) $this->getParam("idcategoria");
    	
    	$model = new Application_Model_Categoria();
    	
    	$flash = $this->_helper->flashMessenger;
    	
		try{
			$model->apagar($idcategoria);
			$flash->addMessage("Registro apagado");
		}catch (Exception $e){
			$flash->addMessage($e->getMessage());
		}
		
		$this->_helper->Redirector->gotoSimpleAndExit('index');
    	
    }

    public function updateAction() {
    	
    	$idcategoria = (int) $this->getParam("idcategoria");
    	
    	$tab = new Application_Model_DbTable_Categoria();
    	$categoria = $tab->fetchRow("idcategoria = $idcategoria");

		if($categoria === NULL){
			
			$flash = $this->_helper->flashMessenger;
			
			$flash->addMessage("Registro nao existe");
			 
			$this->_helper->Redirector->gotoSimpleAndExit('index');
		}
    	
    	$form = new Application_Form_Categoria();
    	 
    	$this->view->form = $form;
    	 
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    	
    		if($form->isValid($post)){
    			$posts = $form->getValues();
    			 
    			$vo = new Application_Model_Vo_Categoria();
    			$vo->setCategoria($post['categoria']);
    			$vo->setIdcategoria($idcategoria);
    			 
    			$model = new Application_Model_Categoria();
    			$model->atualizar($vo);
    			 
    			$flash = $this->_helper->flashMessenger;
    			 
    			$flash->addMessage("Registro atualizado");
    			 
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			 
    		}
    	}else{
    		$form->populate($categoria->toArray());
    	}
    }

}
