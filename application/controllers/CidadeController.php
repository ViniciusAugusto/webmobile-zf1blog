<?php

class CidadeController extends Blog_Controller_Action {

    public function indexAction() {
    	
		$adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$select = $adapter->select("")
		  				  ->from(array("c" => "cidade"),array('id','descricao'))
		  				  ->joinInner(array("e" => "estados"), 'e.idestados = c.idestado', array('nome'))
						  ->order('id DESC');

    	
    	$cidades = $select->query()->fetchAll();
    	
    	
    	$this->view->cidades = $cidades;
    }

    public function createAction() {
    	
        $form = new Application_Form_Cidade();
        
        $this->view->form = $form;
         
        if($this->getRequest()->isPost()){
        	$post = $this->getAllParams();
        
        	if($form->isValid($post)){
        		$cidade = $form->getValues();
        		
        		$auth = Zend_Auth::getInstance();
        		$dados = $auth->getIdentity();
        		 
        		$vo = new Application_Model_Vo_Cidade();
        		$vo->setIdestado($post['idestado']);
        		$vo->setDescricao($post['descricao']);
        		 
        		$model = new Application_Model_Cidade();
        		$model->salvar($vo);
        		 
        		$flash = $this->_helper->flashMessenger;
        		 
        		$flash->addMessage("Registro salvo");
        		 
        		$this->_helper->Redirector->gotoSimpleAndExit('index');
        		 
        	}
        }
    }

    public function deleteAction() {
    	
    	$id = (int) $this->getParam("id");
    	 
    	$model = new Application_Model_Cidade();
    	 
    	$flash = $this->_helper->flashMessenger;
    	 
    	try{
    		$model->apagar($id);
    		$flash->addMessage("Registro apagado");
    	}catch (Exception $e){
    		$flash->addMessage($e->getMessage());
    	}
    	
    	$this->_helper->Redirector->gotoSimpleAndExit('index');
        
    }

    public function updateAction() {
        
    	$id = (int) $this->getParam("id");
    	
    	$tab = new Application_Model_DbTable_Cidade();
    	$cidade = $tab->fetchRow("id = $id");

		if($cidade === NULL){
			
			$flash = $this->_helper->flashMessenger;
			
			$flash->addMessage("Registro nao existe");
			 
			$this->_helper->Redirector->gotoSimpleAndExit('index');
		}
    	
    	$form = new Application_Form_Cidade();
    	 
    	$this->view->form = $form;
    	 
    	if($this->getRequest()->isPost()){
    		$post = $this->getAllParams();
    	
    		if($form->isValid($post)){
    			
    			$post = $form->getValues();
    			 
    			$vo = new Application_Model_Vo_Cidade();
    			$vo->setId($id);
        		$vo->setIdestado($post['idestado']);
        		$vo->setDescricao($post['descricao']);
        		 
        		$model = new Application_Model_Cidade();
        		$model->atualizar($vo);
    			 
    			$flash = $this->_helper->flashMessenger;
    			 
    			$flash->addMessage("Registro atualizado");
    			 
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			 
    		}
    	}else{
    		$form->populate($cidade->toArray());
    	}
    	
    }

}
