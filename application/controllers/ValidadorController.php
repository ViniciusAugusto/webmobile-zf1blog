<?php
class ValidadorController extends Blog_Controller_Action{
	
	public function digitosAction(){
		$v = new Zend_Validate_Digits();
		
		$valor = "fcv";
		
		$resultado = $v->isValid($valor);
		
		if(!$resultado){
			$erros = $v->getMessages();
			echo "<pre>";
			print_r($erros);
		}
	}
	
	public function emailAction(){
		$v = new Zend_Validate_EmailAddress();
		
		$valor = "viniciusaugustocunhagmail.com";

		echo "<pre>";
		
		if(!$v->isValid($valor)){
			print_r($v->getMessages());
		}
		
		exit;
	}
	
	public function betweenAction(){
		$options = array("inclusive" => true, "min" => 18, "max" => 40);
		
		$v = new Zend_Validate_Between($options);
		
		$valor = mt_rand(0, 50);
		
		if(!$v->isValid($valor))
			print_r($v->getMessages());
		
		exit;
	}
	
	public function cadeiaAction(){
		$options = array("min" => 10);
		
		$v = new Zend_Validate();
		$v->addValidator(new Zend_Validate_Alpha(), true);
		$v->addValidator(new Zend_Validate_StringLength());
		
		
		$valor = "123456";
		
		if(!$v->isValid($valor)){
			print_r($v->getMessages());
		}
		
		exit;
		
		
	}	
}
