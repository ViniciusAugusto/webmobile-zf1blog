CREATE DATABASE  IF NOT EXISTS `webmobile` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `webmobile`;
-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: webmobile
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(200) NOT NULL,
  `papel` int(11) NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Admin 01','admin01@email.com','admin01',1),(2,'Admin 02','admin02@email.com','admin02',2);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(200) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Esportes'),(3,'Games'),(4,'LOREM IPSUM 22');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `idestado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cidade_1_idx` (`idestado`),
  CONSTRAINT `fk_cidade_1` FOREIGN KEY (`idestado`) REFERENCES `estados` (`idestados`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` VALUES (3,'Cianorte',3),(4,'Umuarama',3),(5,'Maringa',3),(6,'Sao Paulo',4);
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estados` (
  `idestados` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `uf` char(2) NOT NULL,
  PRIMARY KEY (`idestados`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (3,'Parana','PR'),(4,'Sao Paulo','SP');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `idpost` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `idadmin` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `texto` text,
  PRIMARY KEY (`idpost`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,3,1,'JOGO DE FUTEBOL','Neste ultimo final de semana, os times jogaram ...'),(2,1,2,'Mundial de Voley','O Brasil venceu a seleção da Servia e Montenegro'),(3,3,1,'Lançamento de Call Of Dutty XXI','Nesta semana será lançado o novo jogo da série Call Of Dutty'),(4,4,1,'Lorem Ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac ex eu tortor congue imperdiet posuere at tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. In placerat odio in placerat pellentesque. Aliquam fermentum sed justo vel ultrices. Mauris laoreet risus egestas elit ultrices, quis gravida erat tristique. Sed eget odio in enim efficitur rhoncus. Phasellus sit amet rhoncus tortor. Nulla facilisi.\n\nIn consequat et elit in malesuada. Integer eget turpis metus. Fusce semper justo id fringilla volutpat. Quisque laoreet molestie sollicitudin. Phasellus vulputate aliquet bibendum. Vestibulum imperdiet sapien sed ullamcorper accumsan. Mauris vitae lectus nibh. Integer semper sem non neque consectetur, vel auctor lorem lobortis. Mauris faucibus sagittis ullamcorper. Maecenas aliquet lectus ultricies nisl porta, vitae condimentum lacus congue. Sed interdum, sem eu tristique molestie, elit velit euismod libero, at malesuada sem lorem a urna. Etiam interdum in neque lacinia tincidunt.\n\nMauris ut rutrum massa. Pellentesque quis est ante. Praesent quis efficitur urna. Duis aliquam velit at fringilla interdum. Ut finibus nibh a libero volutpat mollis. Suspendisse vel mauris arcu. Maecenas sed quam sagittis, convallis odio non, dapibus risus. In varius ex urna, nec mattis lacus eleifend sed. Aliquam interdum justo ligula, nec auctor erat vehicula sit amet. Aliquam nec faucibus quam. Fusce arcu erat, ullamcorper sed fermentum a, maximus et urna. Phasellus lacinia diam est, eget auctor urna egestas et. Nunc mi ex, tempor consequat orci eu, mattis sollicitudin velit.\n\nDonec ex dui, maximus ut erat nec, finibus molestie nunc. In consectetur, quam id tristique eleifend, ante erat molestie metus, at tristique magna leo nec diam. Duis vel justo eu nunc malesuada consectetur. Fusce quis ex non risus vulputate ultricies. Donec tristique felis et rhoncus mollis. Sed molestie odio non pellentesque ultricies. Donec mollis massa ante, at elementum mi porttitor ut. Suspendisse pellentesque diam justo, vitae molestie dolor iaculis et. Fusce eu massa a erat blandit suscipit vel ac velit. Donec quam elit, aliquam at bibendum sit amet, iaculis in dolor. Phasellus vitae diam et nisl porta suscipit nec non erat. Sed ultrices gravida lobortis. Maecenas pharetra sapien sed turpis fringilla pretium. Etiam lacus massa, accumsan sed vulputate eu, gravida at ante. Nullam eu est dapibus, ullamcorper felis non, tempus augue.\n\nVivamus faucibus efficitur arcu, at dictum risus euismod vitae. Donec in risus tempor, euismod sem in, rutrum purus. In quis odio convallis felis pulvinar lobortis eget id arcu. Pellentesque tempor, massa nec venenatis tempus, mi leo efficitur urna, nec blandit nunc lacus sed orci. Aliquam scelerisque nibh nec venenatis tincidunt. In quis porta ligula, ac malesuada risus. Suspendisse et suscipit augue. Aliquam erat volutpat. Morbi tincidunt augue vitae justo dapibus, id placerat erat tempor. Aliquam erat volutpat. Nam blandit dui a turpis laoreet, id tincidunt nisi venenatis.'),(5,4,1,'Mussum Ipsum','Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.\n\nSuco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.\n\nCasamentiss faiz malandris se pirulitá, Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer Ispecialista im mé intende tudis nuam golada, vinho, uiski, carirí, rum da jamaikis, só num pode ser mijis. Adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n\nCevadis im ampola pa arma uma pindureta. Nam varius eleifend orci, sed viverra nisl condimentum ut. Donec eget justis enim. Atirei o pau no gatis. Viva Forevis aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Copo furadis é disculpa de babadis, arcu quam euismod magna, bibendum egestas augue arcu ut est. Delegadis gente finis. In sit amet mattis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis. Pellentesque viverra accumsan ipsum elementum gravidis.'),(6,4,1,'Minios Ipsum','Minions ipsum tank yuuu! Poulet tikka masala ti aamoo! Bappleees bappleees poopayee bappleees chasy bananaaaa bananaaaa baboiii. Uuuhhh hahaha gelatooo aaaaaah baboiii la bodaaa wiiiii tatata bala tu baboiii. Uuuhhh gelatooo bananaaaa hana dul sae. Ti aamoo! tulaliloo hana dul sae gelatooo poulet tikka masala butt me want bananaaa! Potatoooo jiji. Hana dul sae aaaaaah jiji poulet tikka masala potatoooo tank yuuu! Tatata bala tu. Chasy ti aamoo! Pepete underweaaar butt belloo! Jeje poopayee bee do bee do bee do. Para tú daa bananaaaa jiji. Underweaaar butt bananaaaa poopayee poulet tikka masala belloo!\n\nPara tú tank yuuu! Pepete uuuhhh bananaaaa bappleees. Poulet tikka masala hahaha baboiii uuuhhh pepete chasy poulet tikka masala hana dul sae. Uuuhhh bappleees bananaaaa baboiii gelatooo jiji la bodaaa potatoooo potatoooo wiiiii. Me want bananaaa! tatata bala tu chasy tulaliloo. Ti aamoo! gelatooo po kass poulet tikka masala underweaaar chasy underweaaar uuuhhh wiiiii bappleees.\n\nMe want bananaaa! tatata bala tu tank yuuu! Me want bananaaa! Pepete la bodaaa gelatooo daa para tú poulet tikka masala. Belloo! jiji bappleees para tú ti aamoo! Daa chasy chasy tatata bala tu. Para tú bananaaaa baboiii la bodaaa tank yuuu! Bappleees tank yuuu! Baboiii wiiiii gelatooo. Para tú la bodaaa uuuhhh tank yuuu! Bananaaaa me want bananaaa! Daa jeje poopayee bananaaaa daa. Tulaliloo hahaha underweaaar para tú chasy. Bananaaaa belloo! Hana dul sae po kass tank yuuu! Hana dul sae bee do bee do bee do daa bee do bee do bee do bananaaaa pepete bappleees. Bappleees bananaaaa jiji jeje bee do bee do bee do.\n\nBappleees wiiiii bananaaaa daa la bodaaa belloo! Para tú tank yuuu! Tulaliloo. Butt bappleees jeje baboiii tank yuuu! Po kass tulaliloo tatata bala tu daa po kass. Aaaaaah bappleees poulet tikka masala chasy bappleees butt bee do bee do bee do tatata bala tu. Daa jiji hana dul sae aaaaaah jiji underweaaar tatata bala tu jiji pepete. Aaaaaah gelatooo poulet tikka masala underweaaar tatata bala tu la bodaaa la bodaaa chasy poopayee. Hahaha poulet tikka masala bappleees po kass jeje me want bananaaa! Chasy hana dul sae jeje. Tulaliloo baboiii bappleees underweaaar bee do bee do bee do butt jiji poulet tikka masala uuuhhh poulet tikka masala gelatooo. Butt po kass me want bananaaa! Baboiii pepete jeje chasy. Jiji baboiii gelatooo tank yuuu! Baboiii belloo! Jeje tulaliloo.'),(7,4,1,'Bacon Ipsum','Bacon ipsum dolor amet ham hock fatback cow, pork kielbasa ball tip boudin beef swine. Landjaeger tri-tip ball tip swine pork pork loin leberkas filet mignon. Pork tongue andouille strip steak frankfurter beef pork belly kevin ball tip ham hock jerky. Pork chop drumstick ground round shoulder pig meatloaf shankle biltong swine fatback porchetta. Doner tri-tip ball tip pork loin shoulder chicken sirloin landjaeger tenderloin filet mignon beef ribs chuck turkey swine. Doner ball tip pork short ribs.\n\nJerky pork loin frankfurter, salami drumstick pastrami shankle kevin pancetta tri-tip beef spare ribs turducken pork belly ham hock. Landjaeger tongue t-bone salami shoulder andouille kevin. Pork loin beef ribs hamburger turkey pig cupim turducken cow sirloin shankle corned beef jowl. Corned beef ribeye porchetta pastrami doner shoulder. Kevin biltong short loin turducken picanha ball tip short ribs pig chicken porchetta. Tongue corned beef bacon pastrami, salami brisket shankle pig rump boudin meatloaf cupim ground round turkey. Pork chop short loin boudin doner, drumstick landjaeger cow strip steak porchetta.\n\nPork jerky corned beef capicola ham tenderloin bacon kielbasa. Biltong pancetta shank pork belly meatball filet mignon alcatra pork pig. Flank pork frankfurter turkey shank porchetta meatball swine ball tip strip steak. Turducken frankfurter spare ribs cow. Chuck ball tip biltong frankfurter rump pork chop, drumstick tenderloin turducken brisket kevin tail shankle.\n\nHamburger ham hock t-bone, jerky andouille frankfurter short ribs porchetta. Tongue rump t-bone kevin turducken, sausage pancetta picanha beef pork belly prosciutto. Sirloin pork cow ham hock. Pork chop biltong turducken, cupim boudin landjaeger brisket fatback sausage filet mignon tri-tip jerky ribeye ham hock. Ground round turkey corned beef tri-tip beef, brisket fatback boudin pork chop. Shank turkey bacon, rump tail tri-tip hamburger cow swine pork beef ribeye.\n\nSausage frankfurter pork loin, bresaola pork chop bacon chuck tongue tenderloin cow kielbasa. Pork belly doner shank jowl tenderloin boudin, bresaola tongue andouille short ribs prosciutto. Leberkas cupim flank spare ribs bacon, ground round pork strip steak rump beef ribs filet mignon sirloin bresaola. Ham hock tri-tip frankfurter doner ribeye hamburger landjaeger shankle t-bone sausage meatball ball tip pig bacon. Ball tip pork pork loin turkey. Jowl turducken frankfurter brisket flank.'),(15,4,1,'BACON IPSUM ASDASDASD ','Bacon ipsum dolor amet ham hock fatback cow, pork kielbasa ball tip boudin beef swine. Landjaeger tri-tip ball tip swine pork pork loin leberkas filet mignon. Pork tongue andouille strip steak frankfurter beef pork belly kevin ball tip ham hock jerky. Pork chop drumstick ground round shoulder pig meatloaf shankle biltong swine fatback porchetta. Doner tri-tip ball tip pork loin shoulder chicken sirloin landjaeger tenderloin filet mignon beef ribs chuck turkey swine. Doner ball tip pork short ribs.\r\n\r\nJerky pork loin frankfurter, salami drumstick pastrami shankle kevin pancetta tri-tip beef spare ribs turducken pork belly ham hock. Landjaeger tongue t-bone salami shoulder andouille kevin. Pork loin beef ribs hamburger turkey pig cupim turducken cow sirloin shankle corned beef jowl. Corned beef ribeye porchetta pastrami doner shoulder. Kevin biltong short loin turducken picanha ball tip short ribs pig chicken porchetta. Tongue corned beef bacon pastrami, salami brisket shankle pig rump boudin meatloaf cupim ground round turkey. Pork chop short loin boudin doner, drumstick landjaeger cow strip steak porchetta.\r\n\r\nPork jerky corned beef capicola ham tenderloin bacon kielbasa. Biltong pancetta shank pork belly meatball filet mignon alcatra pork pig. Flank pork frankfurter turkey shank porchetta meatball swine ball tip strip steak. Turducken frankfurter spare ribs cow. Chuck ball tip biltong frankfurter rump pork chop, drumstick tenderloin turducken brisket kevin tail shankle.\r\n\r\nHamburger ham hock t-bone, jerky andouille frankfurter short ribs porchetta. Tongue rump t-bone kevin turducken, sausage pancetta picanha beef pork belly prosciutto. Sirloin pork cow ham hock. Pork chop biltong turducken, cupim boudin landjaeger brisket fatback sausage filet mignon tri-tip jerky ribeye ham hock. Ground round turkey corned beef tri-tip beef, brisket fatback boudin pork chop. Shank turkey bacon, rump tail tri-tip hamburger cow swine pork beef ribeye.\r\n\r\nSausage frankfurter pork loin, bresaola pork chop bacon chuck tongue tenderloin cow kielbasa. Pork belly doner shank jowl tenderloin boudin, bresaola tongue andouille short ribs prosciutto. Leberkas cupim flank spare ribs bacon, ground round pork strip steak rump beef ribs filet mignon sirloin bresaola. Ham hock tri-tip frankfurter doner ribeye hamburger landjaeger shankle t-bone sausage meatball ball tip pig bacon. Ball tip pork pork loin turkey. Jowl turducken frankfurter brisket flank.'),(16,1,1,'TESTEkmjnkhhj','teste');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-18 13:26:22
